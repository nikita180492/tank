$(document).ready(() => {
  $(".slider-banner").slick(
    {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      adaptiveHeight: false,
      easing: "ease",
      autoplay: true,
      autoplaySpeed: 5000,
      fade: true,
      cssEase: "linear"
    }
  );
  $("[data-fancybox]").fancybox({
    clickContent: "close",
    buttons: ["close"]
  });
  // window.addEventListener("resize", function() {
  //   if (window.innerWidth <= 768) {
  //     $('.slider-banner').slick('unslick');
  //     sliderIsLive = false;
  //   }
  //   else {
  //     if (sliderIsLive) {
  //       $('.slider-banner').slick();
  //       sliderIsLive = true;
  //     }
  //   }
  // });
});


