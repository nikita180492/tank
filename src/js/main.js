// массив иконок svg
$(document).ready(function() {

  const buttons = document.querySelectorAll(".awards-play-block__button");

  buttons.forEach(button => {
    button.addEventListener("click", (event) => {
      event.stopPropagation();
    });
  });

  let x = [".menu-icon"];
  x.forEach(item => {
    $(item).each(function() {
      let $img = $(this);
      let imgClass = $img.attr("class");
      let imgURL = $img.attr("src");
      $.get(imgURL, function(data) {
        let $svg = $(data).find("svg");
        if (typeof imgClass !== "undefined") {
          $svg = $svg.attr("class", imgClass + " replaced-svg");
        }
        $svg = $svg.removeAttr("xmlns:a");
        if (!$svg.attr("viewBox") && $svg.attr("height") && $svg.attr("width")) {
          $svg.attr("viewBox", "0 0 " + $svg.attr("height") + " " + $svg.attr("width"));
        }
        $img.replaceWith($svg);
      }, "");
    });
  });

// language=JQuery-CSS
  $(".hamburger--collapse").click(function() {
    $(this).toggleClass("is-active");
    $(".header-area").toggleClass("header-mobile--active")
    $(".mobile-menu").animate({
      height: "toggle",
      lineHeight: "toggle"
    });
  });
  $("#back-top").click(function() {
    window.scrollTo({ top: 0, behavior: "smooth" });
  });

  $(window).scroll(function() {
    let top = $(this).scrollTop();
    if (top > 150) {
      $("#back-top").addClass("active");
    } else {
      $("#back-top").removeClass("active");
    }
  });
  const menuMobile = () => {
    let headerHeight = $(".header-mobile").outerHeight();
    $(".wrapper-mobile-menu").css("top", headerHeight + "px");
  };
  menuMobile();

  const ebanyiIphone = () => {
    let listTypeButton = document.getElementsByClassName("question-header");
    let q = [...listTypeButton];
    q.forEach(x => {
      x.removeAttribute("type");
    });
  };
  ebanyiIphone();

  const teleportElement = (elPc, elMob, child, size) => {

    let x = elPc.find(child);
    let y = elMob.find(child);
    if ($(window).width() <= size) {
      elMob.append(x);
    }
    if ($(window).width() > size) {
      elPc.append(y);
    }
  };
  $(window).resize(function() {
    menuMobile();
    teleportElement(pcH1, moobileH1, el, 576);
  });
  let pcH1 = $(".pc-h1");
  let moobileH1 = $(".mobile-h1");
  let el = $(".slider-banner-title");
  teleportElement(pcH1, moobileH1, el, 576);

  $(".mobile-menu__li").click(function() {


    if($(this).hasClass("mobile-menu__li--active")){
      $(this).removeClass("mobile-menu__li--active");
      return
    }

    $(".mobile-menu__li").each(function(i, obj) {
      $(obj).removeClass("mobile-menu__li--active");
    });

    $(this).addClass("mobile-menu__li--active");

  });


});

